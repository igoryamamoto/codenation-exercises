
# coding: utf-8

# # Introdução à Ciência de Dados

# ## Numpy

# In[1]:


import numpy as np


# In[2]:


lista = [1,2,2,4]
np.array(lista)


# In[3]:


np.zeros((2,3))


# In[4]:


np.zeros((3,3,3), "int32")


# In[5]:


np.zeros((3,3,3), "int32")[0]


# In[6]:


np.arange(9)


# In[7]:


np.random.uniform(0,1,9)


# In[8]:


array = np.random.randint(0, 10, (1,25))
print array
print "\n"

z = np.ones((5, 5), "int32")
print z


# In[9]:


print array.shape
print z.shape


# In[10]:


matrix = array.reshape(5,5)
print matrix
print "\n"
print array


# In[11]:


print z
z[0,2]


# In[12]:


z[0,2] = 0
print z


# In[13]:


subset = matrix[1:, 2:4]
print subset


# #### Cuidado ao trabalhar com slices
# A variável *subset* acima ainda é uma parte de *matrix*, qualquer alteração nela afeta matrix diretamente

# In[14]:


matrix[0,0] = 100
print(matrix)


# O mesmo acontece com o array original:

# In[15]:


print array


# #### Broadcast
# 

# In[16]:


z + 1


# In[17]:


2*z + 3


# In[18]:


z + [1, 0, 0, 0, 1]  # Replica a linha até ficar do tamanho de z


# In[19]:


z + np.array([1, 0, 0, 0, 1]).reshape(5,1)  # Replica as colunas o quanto for necessário


# In[20]:


z[2] = 2
print z


# In[21]:


coluna = np.ones((6,1), "int")
coluna[5] = 2
linha = np.arange(0,5)
print coluna 
print linha


# In[22]:


linha + coluna


# ### Operações

# In[23]:


np.sum(z, axis=0)  # Soma dos valores de cada coluna


# In[24]:


np.sum(z, axis=1)  # Soma dos valores de cada linha


# In[25]:


np.sum(z)  # Soma total


# In[26]:


np.maximum(z, 2)


# In[27]:


np.minimum(z, 1)


# In[28]:


np.where(z == 0)


# In[29]:


np.where(z > 1)  # Indices de linha, indices de colunas


# In[30]:


np.where(z > 1, 1, 0)


# Para mais referências e exercícios: https://github.com/rougier/numpy-tutorial

# 
# ## Pandas

# In[1]:


import pandas as pd
import seaborn as sns

# Para que os graficos aparecam
get_ipython().magic(u'matplotlib inline')


# In[3]:


# Load one of the data sets that come with seaborn
tips = sns.load_dataset("tips")
tips.head()


# In[33]:


tips.tail()


# In[38]:


tips[20:25]


# In[39]:


tips["size"].tail()


# In[40]:


tips[["total_bill", "sex"]].tail()


# In[41]:


# fumantes
print tips[tips.smoker == "Yes"].head(3)
print '\n'

# gorjetas maiores que 5 E feminino
print tips[(tips.tip > 5) & (tips.sex == 'Female')].head(3)
print '\n'

# gorjetas maiores que 3 OU masculino
print tips[(tips.sex == 'Male') | (tips.size > 3)].head(3)


# In[42]:


(tips.sex == "Male").head()


# In[43]:


print tips.iloc[0]
print '\n'
print tips.iloc[[0, 99]]


# In[34]:


tips.info()


# In[65]:


tips.apply(lambda x: sum(x.isnull()),axis=0) 


# In[35]:


tips.describe()


# In[36]:


tips.corr()


# In[66]:


tips.smoker.value_counts()


# In[67]:


tips.tip.hist()


# In[4]:


tips.total_bill.hist()


# In[73]:


tips.boxplot(column='tip', return_type='axes')


# In[76]:


tips.boxplot(column='tip', by='size', return_type='axes')


# In[82]:


temp1 = tips['size'].value_counts(ascending=True)
temp2 = tips.pivot_table(values='smoker',index=['size'],aggfunc=lambda x: x.map({'Yes':1,'No':0}).mean())
temp3 = pd.crosstab(tips['smoker'], tips["size"])
temp3.plot(kind='bar', stacked=False, grid=False)


# ### Grouping

# In[45]:


por_dia = tips.groupby("day")
por_dia.size()


# In[46]:


por_dia.mean()


# In[47]:


por_dia.sum()


# In[48]:


dia_sexo = tips.groupby(["sex", "day"])
dia_sexo.size()


# In[49]:


dia_sexo.mean()


# ### Seaborn

# Com o seaborn, já temos vários métodos estatísticos na própria biblioteca de gráficos

# In[50]:


sns.lmplot(x="total_bill", y="tip", data=tips);


# In[51]:


sns.lmplot("total_bill", "tip", tips, col="smoker");


# In[52]:


sns.lmplot("total_bill", "tip", tips, col="day");


# In[53]:


sns.lmplot(x="size", y="tip", data=tips);


# In[54]:


sns.lmplot(x="total_bill", y="tip", hue="smoker", data=tips);


# In[55]:


sns.lmplot(x="total_bill", y="tip", hue="smoker", col="time", data=tips);


# In[56]:


sns.jointplot("total_bill", "tip", data=tips, kind='reg');


# ## Criando nosso primeiro modelo

# In[84]:


from sklearn.linear_model import LinearRegression


# In[86]:


tips.head()


# Começando simples, modelo linear com apenas uma variável: $y = ax + b$

# In[88]:


X = tips["size"]
Y = tips.tip


# In[90]:


Y.head()


# In[92]:


print X.shape
print Y.shape


# ### Dividindo em amostra de teste e de treino

# In[99]:


indices_treino = np.random.rand(len(X)) < 0.7


# In[100]:


indices_treino[:5]


# In[101]:


X_treino = X[indices_treino]
y_treino = y[indices_treino]


# In[102]:


X_teste = X[~indices_treino]
y_teste = y[~indices_treino]


# In[103]:


print len(X_treino) + len(X_teste)
print len(X)


# In[115]:


print X_treino.shape, X_teste.shape


# In[122]:


# Passo necessário apenas com uma feature
X_treino = X_treino.reshape(-1, 1)
X_teste = X_teste.reshape(-1, 1)
print X_treino.shape


# Há também um método mais prático alternativo, usando o scikit learn

# In[107]:


from sklearn.cross_validation import train_test_split
X_train, X_test, y_train, y_test = train_test_split(X, Y, random_state=1)


# In[108]:


print len(X_train), len(X_test)


# In[109]:


# Primeiro criamos nosso objeto do modelo
lm = LinearRegression()


# In[119]:


# Treinar é muito simples
lm.fit(X_treino, y_treino)


# In[120]:


# Agora temos a equação da reta
print(lm.intercept_)
print(lm.coef_)


# $y = 0.768 * x + 1.031$

# Agora usamos essa equação pra fazer predições no conjunto de teste

# In[123]:


y_pred = lm.predict(X_teste)


# ### Avaliação

# In[124]:


from sklearn import metrics


# Para regressão a métrica mais comum é RMSE: $$\sqrt{\frac 1n\sum_{i=1}^n(y_i-\hat{y}_i)^2}$$

# In[127]:


true = [100, 50, 30, 20]
pred = [90, 50, 50, 30]


# In[128]:


# Calculando na mão
print(np.sqrt((10**2 + 0**2 + 20**2 + 10**2)/4.))

# Usando scikit-learn
print(np.sqrt(metrics.mean_squared_error(true, pred)))


# Voltando ao nosso modelo:

# In[126]:


print(np.sqrt(metrics.mean_squared_error(y_teste, y_pred)))


# ### Adicionando mais variáveis

# Seguimos mais uma vez o passo a passo. Agora nossa equação no entanto é do tipo $$y = a*X_1 + b*X_2 + c$$

# In[140]:


features = ["size", "total_bill"]
X = tips[features]
Y = tips.tip


# In[136]:


X_treino, X_teste, y_treino, y_teste = train_test_split(X, Y, random_state=1)


# In[139]:


lm2 = LinearRegression()
lm2.fit(X_treino, y_treino)

# Agora temos a equação da reta
print(lm2.intercept_)
print(lm2.coef_)


# In[144]:


list(zip(features, lm2.coef_))


# Assim temos no nosso modelo que $$gorjeta = 0.185 * size + 0.0856 * total\_bill + 0.815$$

# In[146]:


y_pred = lm2.predict(X_teste)
print(np.sqrt(metrics.mean_squared_error(y_teste, y_pred)))


# Temos erro de 1,14667717143 quando usamos as 2 variáveis, sendo que antes tinhamos 0,906401699947. Como erro é uma coisa ruim, o total da conta atrapalhou nosso modelo!!

# In[57]:



from sklearn.datasets import load_iris

# save load_iris() sklearn dataset to iris
# if you'd like to check dataset type use: type(load_iris())
# if you'd like to view list of attributes use: dir(load_iris())
iris = load_iris()

# np.c_ is the numpy concatenate function
# which is used to concat iris['data'] and iris['target'] arrays 
# for pandas column argument: concat iris['feature_names'] list
# and string list (in this case one string); you can make this anything you'd like..  
# the original dataset would probably call this ['Species']
data1 = pd.DataFrame(data= np.c_[iris['data'], iris['target']],
                     columns= iris['feature_names'] + ['target'])


# In[58]:


data1.head()


# In[59]:


data1.tail()


# In[60]:


data1


# In[61]:


data1.describe()


# In[62]:


colunas = data1.columns


# In[63]:


get_ipython().magic(u'matplotlib inline')
import matplotlib.pyplot as plt
data1["target"].hist()


# In[64]:


data1["petal width (cm)"].hist(bins=20)


# In[3]:


import IPython


# In[4]:


IPython.core


# In[ ]:




