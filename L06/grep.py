# -*- coding: utf-8 -*-

import argparse
import filetools as ft


def find_string(string, path):
    content = ft.get_clean_content(path)
    lines_with_string = []
    for i, val in enumerate(content):
        if string in val:
            lines_with_string.append(i+1)
    return lines_with_string
    
def find_not_string(string, path):
    content = ft.get_clean_content(path)
    lines_with_string = []
    for i, val in enumerate(content):
        if string not in val:
            lines_with_string.append(i+1)
    return lines_with_string
    
if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='Implementation of grep function')    
    parser.add_argument('string', 
                        type=str, 
                        help='String to be found')
    parser.add_argument('filepath',
                        type=str,
                        help='File in which the word will be searched')
    parser.add_argument('-v',
                        help='Get lines in wich string doesnt appear')
    args = parser.parse_args()
    string = args.string
    path = args.filepath
    if args.v:
        lines = find_not_string(string, path)
    else:
        lines = find_string(string, path)
    print(lines)
