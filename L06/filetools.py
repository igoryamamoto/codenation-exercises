def get_content(path):
    with open(path) as f:
        content = f.readlines()
    return content

def get_clean_content(path):
    with open(path) as f:
        content = f.readlines()
    return [x.strip() for x in content]
