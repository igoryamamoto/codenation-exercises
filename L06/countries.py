# -*- coding: utf-8 -*-
import requests
from IPython.display import SVG


def get_countries_info():
    response = requests.get('https://restcountries.eu/rest/v2/all')
    return response.json()
    
def get_countries():
    countries_list = get_countries_info()
    return [{'alpha3Code':x['alpha3Code'], 'name':x['name'],'region':x['region'],'population':x['population'],'area':x['area']} for x in countries_list]

def get_countries_by_continent(continent):
    countries_list = get_countries_info()
    countries_in_continent = filter(lambda x: x['region'] == continent,
                                    countries_list)
    return [{'alpha3Code':x['alpha3Code'], 'name':x['name'],'region':x['region'],'population':x['population'],'area':x['area']} for x in countries_in_continent]
    
def get_countries_min_population(population):
    countries_list = get_countries_info()
    countries_min_population = filter(lambda x: x['population'] >= population,
                                    countries_list)
    return [{'alpha3Code':x['alpha3Code'], 'name':x['name'],'region':x['region'],'population':x['population'],'area':x['area']} for x in countries_min_population]
    
def get_countries_filter(key, value):
    countries_list = get_countries_info()
    countries_filter = filter(lambda x: x[key] == value,
                                    countries_list)
    return [{'alpha3Code':x['alpha3Code'], 'name':x['name'],'region':x['region'],'population':x['population'],'area':x['area']} for x in countries_filter]
    
def get_country_flag(country_name):
    countries_list = get_countries_info()
    country = filter(lambda x: x['name'] == country_name, countries_list)[0]
    flagURL = country['flag']
    return SVG(url=flagURL)
    
def continent_report(continent):
    continent_list = get_countries_by_continent(continent)
    countries_num = len(continent_list)
    total_population = sum([x['population'] for x in continent_list if x['population']])
    total_area = sum([x['area'] for x in continent_list if x['area']])
    return {'number':countries_num, 'population':total_population, 'area':total_area}

 
r1 = get_countries()    
r2 = get_countries_by_continent('Americas')
r3 = get_countries_min_population(100000000)
r4 = continent_report('Europe')
r5 = get_country_flag('Brazil')
r6 = get_countries_filter('population', 206135893)
