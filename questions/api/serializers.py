from rest_framework import serializers
from polls.models import Question

class QuestionSerializer(serializers.ModelSerializer):

    class Meta:
        model = Question
        fields = ('id', 'question_text', 'pub_date', 'create_date')
        read_only_fields = ('create_date',)
