from django.conf.urls import url, include
from rest_framework.urlpatterns import format_suffix_patterns
from .views import CreateView, DetailsView
from rest_framework.authtoken import views

urlpatterns = {
    url(r'^auth/', include('rest_framework.urls',
        namespace='rest_framework')), # Adicione esta linha
    url(r'^questions/$', CreateView.as_view(), name="create"),
    url(r'^questions/(?P<pk>[0-9]+)/$',
     DetailsView.as_view(), name="questions_details"),
    url(r'^token/', views.obtain_auth_token),
}

# Adiciona os sufixos de URL aceitos aos nossos urlpatterns.
# Por padrao adiciona html e json.
urlpatterns = format_suffix_patterns(urlpatterns)
