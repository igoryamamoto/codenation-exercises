from rest_framework.permissions import BasePermission
from polls.models import Question

class IsAPIUser(BasePermission):
    def has_object_permission(self, request, view, obj):
        """Retorna True se o usuario faz parte do grupo api-users."""
        return request.user.groups.filter(name='api-users').exists()
