from __future__ import unicode_literals

from django.db import models
from django import utils
import datetime

from django.contrib.auth.models import User

class Question(models.Model):
    question_text = models.CharField(max_length=200)
    create_date = models.DateTimeField('date created', default=utils.timezone.now)
    pub_date = models.DateTimeField('date published')

    def was_published_recently(self):
        return self.pub_date >= utils.timezone.now() - datetime.timedelta(days=1)

    def __str__(self):
        return self.question_text.encode('utf-8')

class Choice(models.Model):
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    choice_text = models.CharField(max_length=200)

    class Meta:
        permissions = (
            ("user_can_vote", "User can vote on choice"),
        )

    def __str__(self):
        return self.choice_text.encode('utf-8')

class Vote(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    choice = models.ForeignKey(Choice, on_delete=models.CASCADE)

    def __str__(self):
        return ' - '.join([self.user.username, str(self.choice.question),
                       str(self.choice)]).encode('utf-8')
