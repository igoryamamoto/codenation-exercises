# -*- coding: utf-8 -*-
"""
Created on Mon May 22 20:51:25 2017

@author: peixeboi
"""

#%%
from sqlalchemy import create_engine
engine = create_engine('sqlite:///codenation.db', echo=True)

#%%
from sqlalchemy.ext.declarative import declarative_base
Base = declarative_base()

#%%
from sqlalchemy import Column, Integer, String
class User(Base):
    __tablename__ = 'users'
    id = Column(Integer, primary_key=True)
    name = Column(String)
    fullname = Column(String)
    password = Column(String)
    def __repr__(self):
       return "<User(name='%s', fullname='%s', password='%s')>" % (
                            self.name, self.fullname, self.password)
                            
                            
Base.metadata.create_all(engine)

igor = User(name="Igor",fullname="Igor Yamamoto",password="123")

#%%
from sqlalchemy.orm import sessionmaker
Session = sessionmaker(bind=engine)
session = Session()

#%%
session.add(igor)
session.query(User).all()

#%%
session.add_all([
    User(name='wendy', fullname='Wendy Williams', password='foobar'),
    User(name='mary', fullname='Mary Contrary', password='xxg527'),
    User(name='fred', fullname='Fred Flinstone', password='blah')])

#%%
igor.password = 'f8s7ccs'
session.dirty
session.new
session.commit()

#%%
for instance in session.query(User).order_by(User.id):
    print instance.name, instance.fullname
    
#%%
for name, fullname in session.query(User.name, User.fullname):
    print(name, fullname)

#%%
for row in session.query(User.name.label('name_label')).all():
    print(row.name_label)

#%%
for u in session.query(User).order_by(User.id).limit(2).offset(1):
    print u

#%%
for u in session.query(User).order_by(User.id)[1:3]:
    print u

#%%
for user_id, name in session.query(User.id, User.name).\
            filter_by(fullname='Igor Yamamoto'):
    print user_id, name
    
#%%
for name, in session.query(User.name).\
        filter(User.fullname=='Ed Jones'):
    print(name)

#%%
for user in session.query(User).\
          filter(User.name=='Igor').\
          filter(User.fullname=='Igor Yamamoto'):
    print(user)

#%%


    
    
    
    
    
    
    













