# -*- coding: utf-8 -*-
import scrapy
from news_bot.items import NewsBotItem


class MpfSpider(scrapy.Spider):
    name = 'mpf'
    allowed_domains = ['www.mpf.mp.br']
    start_urls = ['http://www.mpf.mp.br/pgr/noticias-pgr/']

    def parse(self, response):
        for article in response.xpath("//article"):
            item = NewsBotItem()
            item['title'] = article.xpath(".//h2/a/text()").extract_first().strip()
            item['category'] = article.xpath(".//div[@class='categoria']/span/text()").extract_first().strip()
            item['headline'] = article.xpath(".//p/text()").extract_first().strip()
            item['link'] = article.xpath(".//h2/a/@href").extract_first().strip()
            item['date'] = article.xpath(".//div[@class='categoria']/span[@class='data']/text()").extract_first().strip()
            yield item
