import sys
import filetools as ft


def sort_words(path):
    """Sort words from a file.

    Parameters
    ----------
    path : string
        Path to the file to be sorted.

    Returns
    -------
    content : list
        Sorted list of words from original file.

    See Also
    --------
    uniq

    """
    content = ft.get_clean_content(path)
    return sorted(content)

if __name__ == '__main__':
    path = sys.argv[1]
    content = sort_words(path)
    for x in content:
        print(x)
