import sys
import filetools as ft

def uniq_words(path):
    """Remove duplicated words from a file.
    
    Parameters
    ----------
    path : string
        Path to the file.
    
    Returns
    -------
    content : list
        List of unique words from original file.
    
    See Also
    --------
    sort_file
    
    Notes
    -----
    When we transform the data structure to a 'set',
    python removes duplicity.
    
    """
    content = ft.get_clean_content(path)
    return list(set(content))

if __name__ == '__main__':
    path = sys.argv[1]
    content = uniq_words(path)
    content.sort()
    for x in content:
        print(x)
