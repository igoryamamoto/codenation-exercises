# -*- coding: utf-8 -*-
"""
Created on Mon May 15 20:50:32 2017

@author: peixeboi
"""
import sys
import filetools as ft


def count_lines(path):
    """Count number of a file.

    Parameters
    ----------
    path : string
        Path to the file to be sorted.

    Returns
    -------
    num_lines : list
        Number of lines of file

    See Also
    --------
    uniq, sort_file

    """
    content = ft.get_clean_content(path)
    return len(content)

if __name__ == '__main__':
    path = sys.argv[1]
    lines = (path)
    for x in lines:
        print(x)