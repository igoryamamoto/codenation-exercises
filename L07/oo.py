# -*- coding: utf-8 -*-

class Customer(object):
    
    initial_balance = 0
    
    def __init__(self, name):
        self.name = name
        self.balance = Customer.initial_balance
        
    def deposit(self, amount):
        self.balance += amount
        
#%% Herança
        
class Egg(object):
    def altered(self):
        print "I am an egg"
        
class Chicken(Egg):
    def altered(self):
        print "Chicken comes before the egg"
        super(Chicken, self).altered()
        print "Now Chicken comes after the egg"
        
dad = Egg()
son = Chicken()
dad.altered()
son.altered()

#%% Herança Multipla

class A(object):
    def __init__(self):
        super(A, self).__init__()
        self.a = 1
        
class B(object):
    def __init__(self):
        super(B, self).__init__()
        self.b = 2
        self.a = 'surprise'
        
class C(B,A):
    def __init__(self):
        super(C, self).__init__()

#%% Composição

class Other(object):
    def __init__(self):
        super(Other, self).__init__()
        self.something = 1
class A(object):
    def __init__(self):
        self.other = Other()
    def something(self):
        return self.other.something

#%% Customização

class Cat(object):
    def __init__(self, name):
        self.name = name
        
    def __repr__(self):
        return "{}".format(self.name)
        
class Dog(object):
    def __init__(self, name):
        self.name = name
    
    def __eq__(self, other):
        if getattr(other, 'name', None) == self.name:
            return True
        return False
        
mycat = Cat('Tom')
mydog = Dog('Tom')
mycat == mydog
print mycat
