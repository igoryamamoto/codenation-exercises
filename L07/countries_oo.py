# -*- coding: utf-8 -*-

class CountriesDB(object):
    def request_api(self):
        response = requests.get('https://restcountries.eu/rest/v2/all')
        return response.json()
    def write_data(self):
        countries = self.request_api
    def save_data(self):
        
class Country(object):
    def __init__(self, *args):
        self.alpha3code,
        self.name,
        self.region,
        self.population,
        self.area = args